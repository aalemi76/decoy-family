//
//  CharacterTableViewCell.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import UIKit

class CharacterTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: CharacterTableViewCell.self)
    
    var viewModel: Reusable?
    
    var status: String = "" {
        didSet {
            if status == "alive" {
                statusBtn.backgroundColor = GlobalSettings.shared().mainColor
            } else if status == "dead" {
                statusBtn.backgroundColor = GlobalSettings.shared().lightRed
            } else {
                statusBtn.backgroundColor = GlobalSettings.shared().darkGray
            }
            statusLabel.text = status
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusBtn: UIView!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var firsSeenLabel: UILabel!
    @IBOutlet weak var lastSeenLabel: UILabel!
    @IBOutlet weak var archiveButton: UIImageView!
    
    
    
    @IBOutlet weak var characterImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        statusBtn.layer.cornerRadius = 10
        statusBtn.layer.masksToBounds = true
        characterImage.layer.cornerRadius = 20
        characterImage.contentMode = .scaleAspectFill
        archiveButton.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didSelectArchiveButton))
        archiveButton.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
        viewModel.cellDidLoad(self)
        return
    }
    
    func update(model: Any) {
        if let model = model as? CharactersQuery.Data.Character.Result {
            nameLabel.text = model.name
            status = model.status?.lowercased() ?? "Unknown"
            speciesLabel.text = model.species?.lowercased() ?? "Unknown"
            firsSeenLabel.text = model.episode.first??.name ?? "Unknown"
            lastSeenLabel.text = model.location?.name ?? "Unknown"
            characterImage.loadImage(url: model.image ?? "", placeHolder: UIImage(named: "CharacterPlaceHolder"))
        } else if let model = model as? CharactersByIdsQuery.Data.CharactersById {
            nameLabel.text = model.name
            status = model.status?.lowercased() ?? "Unknown"
            speciesLabel.text = model.species?.lowercased() ?? "Unknown"
            firsSeenLabel.text = model.episode.first??.name ?? "Unknown"
            lastSeenLabel.text = model.location?.name ?? "Unknown"
            characterImage.loadImage(url: model.image ?? "", placeHolder: UIImage(named: "CharacterPlaceHolder"))
        }
        return
    }
    
    func updateArchiveStatus(newState: Bool) {
        if newState  {
            archiveButton.image = UIImage(named: "archiveFilled")
        } else {
            archiveButton.image = UIImage(named: "archive")
        }
    }
    
    @objc func didSelectArchiveButton() {
        (viewModel as? CharacterCellViewModel)?.didSelectArchiveButton()
    }
    
}
