//
//  CharactersListViewModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import Foundation

class CharactersListViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    var page = 0
    
    var cells = [TableCellViewModel]()
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        fetchItems()
    }
    
    func fetchItems() {
        
        //TODO: map fetched items to swift objects
        
        page += 1
        
        NetworkManager.shared.apollo.fetch(query: CharactersQuery(page: page, filter: nil)) { [weak self] response in
            
            switch response {
            case .success(let result):
                guard let items = result.data?.characters?.results else { return }
                let sections = self?.makeSections(items: items)
                if self?.page == 1 {
                    self?.view?.show(result: .success(sections ?? ""))
                } else {
                    self?.view?.loadNextPage(result: .success(sections?[0].getCells() ?? ""))
                }
            case .failure(let error):
                print(error)
                let error = RMError(rawValue: error.localizedDescription)
                self?.view?.show(result: .failure(error ?? .serverResponse))
            }
            
        }

        
    }
    
    func makeSections(items: [CharactersQuery.Data.Character.Result?]) -> [Sectionable] {
        
        let newCells = items.map({CharacterCellViewModel(reuseID: CharacterTableViewCell.reuseID, cellClass: CharacterTableViewCell.self, model: $0)})
        
        let sections = [SectionProvider(cells: newCells, headerView: nil, footerView: nil)]
        
        cells += newCells
        
        return sections
        
        
    }
    
}
