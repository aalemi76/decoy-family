//
//  ListViewController.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-13.
//

import UIKit
import RxSwift

class ListViewController: SharedViewController {
    
    var indexPath = IndexPath()
    
    let disposeBag = DisposeBag()
    
    let sectionImageName: String
    
    let sectionHeaderHeight: CGFloat
    
    let tableRowHeight: CGFloat
    
    let viewModel: ViewModelProvider
    
    let pinToTop: Bool
    
    lazy var listHeaderView: ListSectionHeader = {
        let view = ListSectionHeader(viewHeight: sectionHeaderHeight, frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: sectionHeaderHeight), imageName: sectionImageName)
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .plain)
        tbl.backgroundColor = .white
        tbl.sectionHeaderHeight = 0
        tbl.sectionFooterHeight = 0
        tbl.rowHeight = tableRowHeight
        tbl.tableFooterView = nil
        tbl.tableHeaderView = listHeaderView
        tbl.contentInsetAdjustmentBehavior = .always
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        return container
    }()
    
    lazy var upBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "up"), for: .normal)
        btn.addTarget(self, action: #selector(didSelectUpBtn), for: .touchUpInside)
        btn.tintColor = GlobalSettings.shared().mainColor
        return btn
    }()
    
    init(viewModel: ViewModelProvider, tableRowHeight: CGFloat, sectionHeaderHeight: CGFloat, name: String, sectionImageName: String, pinToTop: Bool) {
        self.viewModel = viewModel
        self.tableRowHeight = tableRowHeight
        self.sectionHeaderHeight = sectionHeaderHeight
        self.sectionImageName = sectionImageName
        self.pinToTop = pinToTop
        super.init(nibName: nil, bundle: nil)
        self.title = name
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func didSelectUpBtn() {
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addTableView()
        viewModel.viewDidLoad(self)
        showLoadingView()
        listHeaderView.setupLayer()
        configureUpButton()
    }
    
    func addTableView() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        if pinToTop {
            tableViewContainer.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        } else {
            tableViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        }
    }
    
    func configureUpButton() {
        upBtn.isHidden = true
        upBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(upBtn)
        NSLayoutConstraint.activate([
            upBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -15),
            upBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            upBtn.widthAnchor.constraint(equalToConstant: 80),
            upBtn.heightAnchor.constraint(equalToConstant: 80)])
    }

}
