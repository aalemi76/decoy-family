//
//  CharactersHeader.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import UIKit

class ListSectionHeader: UIView {

    let viewHeight: CGFloat
    let imageName: String
    
    private lazy var animator = UIViewPropertyAnimator(duration: 3, curve: .easeIn)
    
    let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
    
    private var imageViewHeight = NSLayoutConstraint()
    private var imageViewBottom = NSLayoutConstraint()
    
    private var containerViewHeight = NSLayoutConstraint()
    
    private var containerView = UIView()
    
    private lazy var imageView: UIImageView = {
        let img = UIImageView(image: UIImage(named: imageName))
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        return img
    }()
    
    init(viewHeight: Double, frame: CGRect, imageName: String) {
        
        self.viewHeight = CGFloat(viewHeight)
        self.imageName = imageName
        super.init(frame: frame)
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        
        self.viewHeight = 0
        self.imageName = ""
        super.init(coder: coder)
        setConstraints()
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func setConstraints() {
        
        addSubview(containerView)
        containerView.addSubview(imageView)
        
//         UIView Constraints
        NSLayoutConstraint.activate([
            containerView.widthAnchor.constraint(equalTo: widthAnchor),
            self.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            containerView.heightAnchor.constraint(equalTo: heightAnchor)
        ])
        
        // Container View Constraints
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        containerViewHeight = containerView.heightAnchor.constraint(equalTo: self.heightAnchor)
        containerViewHeight.isActive = true
        
        // ImageView Constraints
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageViewBottom = imageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        imageViewBottom.isActive = true
        imageViewHeight = imageView.heightAnchor.constraint(equalTo: containerView.heightAnchor)
        imageViewHeight.isActive = true
    }
    
    func setupLayer() {
        
        let path = getPath(viewHeight: Double(viewHeight))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = path.cgPath
        imageView.layer.mask = maskLayer
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = maskLayer.path
        borderLayer.fillColor = UIColor.clear.cgColor
        imageView.layer.addSublayer(borderLayer)
    }
    
    func getPath(viewHeight: Double) -> UIBezierPath {
        
        let path = UIBezierPath(rect: frame)
        path.move(to: CGPoint(x: UIScreen.main.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 2*viewHeight/3))
        path.addCurve(to: CGPoint(x: UIScreen.main.bounds.width, y: viewHeight - 50), controlPoint1: CGPoint(x: UIScreen.main.bounds.width/2 - 50, y: 2*viewHeight/3 - 50), controlPoint2: CGPoint(x: UIScreen.main.bounds.width/2 + 50, y: viewHeight))
        path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: 0))
        path.close()
        
        return path
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        containerViewHeight.constant = scrollView.contentInset.top
        let offsetY = -(scrollView.contentOffset.y + scrollView.contentInset.top)
        containerView.clipsToBounds = offsetY <= 0
        imageViewBottom.constant = offsetY >= 0 ? 0 : -offsetY / 2
        imageViewHeight.constant = max(offsetY + scrollView.contentInset.top, scrollView.contentInset.top)
        
        
//        if scrollView.contentOffset.y > 0 {
//            animator.fractionComplete = 0
//        } else {
//            animator.fractionComplete = abs(scrollView.contentOffset.y) / 100
//        }
    }
    
    func setupVisualEffects() {
        if #available(iOS 13, *) {
            imageView.addSubview(visualEffectView)
            visualEffectView.pinToEdge(imageView)
            
            animator.addAnimations { [weak self] in
                self?.visualEffectView.effect = nil
            }
            
            animator.isReversed = true
            animator.fractionComplete = 0
            
        } else {
            animator.addAnimations {[weak self] in
                guard let self = self else { return }
                
                let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
                self.imageView.addSubview(visualEffectView)
                visualEffectView.pinToEdge(self.imageView)
                
            }
        }
    }

}
