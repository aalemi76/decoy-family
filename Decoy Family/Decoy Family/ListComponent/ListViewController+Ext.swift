//
//  ListViewController+Ext.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-13.
//

import UIKit

extension ListViewController: Viewable {
    
    func show(result: Result<Any, RMError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                
                guard let sections = data as? [Sectionable] else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return}
                self?.configureTableView(sections)
                self?.subscribeToScroll()
                
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
                self?.dismissLoadingView()
            }
            
        }

    }
    
    func loadNextPage(result: Result<Any, RMError>) {
        DispatchQueue.main.async { [weak self] in
            switch result {
            case .success(let data):
                
                guard let rows = data as? [Reusable] else {
                    self?.showErrorBanner(title: "An error occured while loading posts.")
                    return}
                self?.tableViewContainer.loadNextPage(rows, from: self?.indexPath)
                
            case .failure(let error):
                self?.showErrorBanner(title: error.rawValue)
                self?.dismissLoadingView()
            }
            
        }
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        subscribeOnTap()
        subscribeToLoadNextPage()
        dismissLoadingView()
    }
    
    func subscribeOnTap() {
        tableViewContainer.delegateHandler.passSelectedItem.subscribe { [weak self] (event) in
            guard let self = self, let model = event.element as? CharactersQuery.Data.Character.Result else { return }
        }.disposed(by: disposeBag)
    }
    
    func subscribeToScroll() {
        tableViewContainer.delegateHandler.scrollViewDidScroll.subscribe({ [weak self] (event) in
            self?.scrollViewDidScroll(event.element)
        }).disposed(by: disposeBag)
    }
    
    func subscribeToLoadNextPage() {
        tableViewContainer.dataSourceHandler.loadNextPage.subscribe { [weak self] (event) in
            guard let indexPath = event.element else { return }
            self?.indexPath = indexPath
            self?.viewModel.fetchItems()
        }.disposed(by: disposeBag)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView?) {
        guard let scrollView = scrollView else { return }
        listHeaderView.scrollViewDidScroll(scrollView: scrollView)
        if scrollView.contentOffset.y > 400 {
            upBtn.isHidden = false
        } else {
            upBtn.isHidden = true
        }
    }
    
}
