//
//  EpisodeTableViewCell.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-13.
//

import UIKit

class EpisodeTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: EpisodeTableViewCell.self)
    
    var viewModel: Reusable?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var episodeLabel: UILabel!
    @IBOutlet weak var airDateLabel: UILabel!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView3: UIImageView!
    @IBOutlet weak var imgView4: UIImageView!
    @IBOutlet weak var archiveButton: UIImageView!
    
    private lazy var images: [UIImageView] = {
        return [imgView1, imgView2, imgView3, imgView4]
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        images.forEach { img in
            img.layer.cornerRadius = 20
            img.contentMode = .scaleAspectFill
        }
        archiveButton.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didSelectArchiveButton))
        archiveButton.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
        viewModel.cellDidLoad(self)
        return
    }
    
    func update(model: Any) {
        if let model = model as? EpisodesQuery.Data.Episode.Result {
            nameLabel.text = model.name ?? "Unknown"
            episodeLabel.text = model.episode ?? "Unknown"
            airDateLabel.text = model.airDate ?? "Unknown"
            let urls = model.characters.map({ $0?.image })
            setImages(urls: urls)
        } else if let model = model as? EpisodesByIdsQuery.Data.EpisodesById {
            nameLabel.text = model.name ?? "Unknown"
            episodeLabel.text = model.episode ?? "Unknown"
            airDateLabel.text = model.airDate ?? "Unknown"
            let urls = model.characters.map({ $0?.image })
            setImages(urls: urls)
        }
        return
    }
    
    func setImages(urls: [String?]) {
        var imgCount = 3
        if (urls.count - 1) >= 3 {
            imgCount = 3
        } else if (urls.count - 1) <= 0 {
            for image in images {
                image.image = UIImage(named: "CharacterPlaceHolder")
            }
            return
        } else {
            imgCount = (urls.count - 1)
        }
        for i in 0...imgCount {
            images[i].loadImage(url: urls[i] ?? "", placeHolder: UIImage(named: "CharacterPlaceHolder"))
        }
    }
    
    func updateArchiveStatus(newState: Bool) {
        if newState  {
            archiveButton.image = UIImage(named: "archiveFilled")
        } else {
            archiveButton.image = UIImage(named: "archive")
        }
    }
    
    @objc func didSelectArchiveButton() {
        (viewModel as? EpisodeCellViewModel)?.didSelectArchiveButton()
    }
    
}
