//
//  EpisodeCellViewModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import Foundation

class EpisodeCellViewModel: TableCellViewModel {
    
    weak var cell: Updatable?
    
    override func cellDidLoad(_ cell: Updatable) {
        
        self.cell = cell
        
        //TODO: check for archive status
        
    }
    
    func didSelectArchiveButton() {
        
        //TODO: change archive status of model
    }
}
