//
//  ParchmentCoordinator.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-13.
//

import UIKit

class ParchmentCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.childCoordinators = []
        self.navigationController = navigationController
    }
    
    @discardableResult func start() -> UIViewController {
        let vc = LaunchScreenViewController()
        vc.delegate = self
        push(viewController: vc, animated: true)
        return navigationController
    }
    
    func launchParchmentVC() {
        let archiveListViewModel = ArchiveListViewModel()
        archiveListViewModel.delegate = self
        let viewControllers = [
            makeListVC(viewModel: CharactersListViewModel(), tableRowHeight: 200, sectionHeaderHeight: 230, title: "Characters", headerImageName: "CharactersHeader"),
            makeListVC(viewModel: LocationsListViewModel(), tableRowHeight: 320, sectionHeaderHeight: 230, title: "Locations", headerImageName: "LocationsHeader"),
            makeListVC(viewModel: EpisodeListViewModel(), tableRowHeight: 320, sectionHeaderHeight: 230, title: "Episodes", headerImageName: "EpisodesHeader"),
            makeListVC(viewModel: archiveListViewModel, tableRowHeight: 500, sectionHeaderHeight: 230, title: "Archive", headerImageName: "ArchiveHeader", allowsSelection: false)
        ]
        let vc = ParchmentViewController(viewControllers: viewControllers)
        vc.view.backgroundColor = .white
        vc.navigationItem.title = "Decoy Family"
        push(viewController: vc, animated: true)
        navigationController.viewControllers.remove(at: 0)
    }
    
    func makeListVC(viewModel: ViewModelProvider, tableRowHeight: CGFloat, sectionHeaderHeight: CGFloat, title: String, headerImageName: String, pinToTop: Bool = true, allowsSelection: Bool = true) -> SharedViewController {
        let vc = ListViewController(viewModel: viewModel, tableRowHeight: tableRowHeight, sectionHeaderHeight: sectionHeaderHeight, name: title, sectionImageName: headerImageName, pinToTop: pinToTop)
        vc.view.backgroundColor = .white
        vc.tableViewContainer.tableView.allowsSelection = allowsSelection
        return vc
    }
    
}
