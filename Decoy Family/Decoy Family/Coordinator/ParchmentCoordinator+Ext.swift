//
//  ParchmentCoordinator+Ext.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-16.
//

import Foundation

extension ParchmentCoordinator: LaunchScreenViewControllerDelegate {
    
    func launchParchmentViewController() {
        launchParchmentVC()
    }
}

extension ParchmentCoordinator: ArchiveListViewModelDelegate {
    
    func didSelectCharactersBtn() {
        let vc = makeListVC(viewModel: ArchiveCharactersViewModel(), tableRowHeight: 200, sectionHeaderHeight: 230, title: "Characters", headerImageName: "CharactersHeader", pinToTop: false)
        vc.view.backgroundColor = .white
        vc.navigationItem.title = "Characters"
        push(viewController: vc, animated: true)
    }
    
    func didSelectLocationsBtn() {
        let vc = makeListVC(viewModel: ArchivedLocationsViewModel(), tableRowHeight: 320, sectionHeaderHeight: 230, title: "Locations", headerImageName: "LocationsHeader", pinToTop: false)
        vc.view.backgroundColor = .white
        vc.navigationItem.title = "Locations"
        push(viewController: vc, animated: true)
    }
    
    func didSelectEpisodesBtn() {
        let vc = makeListVC(viewModel: ArchivedEpisodesViewModel(), tableRowHeight: 320, sectionHeaderHeight: 230, title: "Episodes", headerImageName: "EpisodesHeader", pinToTop: false)
        vc.view.backgroundColor = .white
        vc.navigationItem.title = "Locations"
        push(viewController: vc, animated: true)
    }
    
    
}
