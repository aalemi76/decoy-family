//
//  LocationsTableViewCell.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-13.
//

import UIKit

class LocationsTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: LocationsTableViewCell.self)
    
    var viewModel: Reusable?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dimentionLabel: UILabel!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView3: UIImageView!
    @IBOutlet weak var imgView4: UIImageView!
    @IBOutlet weak var archiveButton: UIImageView!
    
    private lazy var images: [UIImageView] = {
        return [imgView1, imgView2, imgView3, imgView4]
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        for img in images {
            img.layer.cornerRadius = 20
            img.contentMode = .scaleAspectFill
        }
        archiveButton.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didSelectArchiveButton))
        archiveButton.addGestureRecognizer(tapGestureRecognizer)
    }

    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
        viewModel.cellDidLoad(self)
        return
    }
    
    func update(model: Any) {
        if let model = model as? LocationsQuery.Data.Location.Result {
            nameLabel.text = model.name ?? "Unknown"
            typeLabel.text = "Type: \(model.type?.lowercased() ?? "unknown")"
            dimentionLabel.text = "Dimension: \(model.dimension?.lowercased() ?? "unknown")"
            let urls = model.residents.map({ $0?.image })
            setImages(urls: urls)
        } else if let model = model as? LocationsByIdsQuery.Data.LocationsById {
            nameLabel.text = model.name ?? "Unknown"
            typeLabel.text = "Type: \(model.type?.lowercased() ?? "unknown")"
            dimentionLabel.text = "Dimension: \(model.dimension?.lowercased() ?? "unknown")"
            let urls = model.residents.map({ $0?.image })
            setImages(urls: urls)
        }
    }
    
    func setImages(urls: [String?]) {
        var imgCount = 3
        if (urls.count - 1) >= 3 {
            imgCount = 3
        } else if (urls.count - 1) <= 0 {
            for image in images {
                image.image = UIImage(named: "CharacterPlaceHolder")
            }
            return
        } else {
            imgCount = (urls.count - 1)
        }
        for i in 0...imgCount {
            images[i].loadImage(url: urls[i] ?? "", placeHolder: UIImage(named: "CharacterPlaceHolder"))
        }
    }
    
    func updateArchiveStatus(newState: Bool) {
        if newState  {
            archiveButton.image = UIImage(named: "archiveFilled")
        } else {
            archiveButton.image = UIImage(named: "archive")
        }
    }
    
    @objc func didSelectArchiveButton() {
        (viewModel as? LocationCellViewModel)?.didSelectArchiveButton()
    }
    
}
