//
//  ParchmentViewController.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-13.
//

import UIKit
import Parchment

class ParchmentViewController: UIViewController {
    
    var viewControllers: [SharedViewController]
    
    private lazy var pagingViewController: PagingViewController = {
        let vc = PagingViewController(viewControllers: viewControllers)
        return vc
    }()
    
    init(viewControllers: [SharedViewController]) {
        self.viewControllers = viewControllers
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutPagingVC()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func layoutPagingVC() {
        addChild(pagingViewController)
        view.addSubview(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
        pagingViewController.textColor = GlobalSettings.shared().darkGray
        pagingViewController.selectedTextColor = GlobalSettings.shared().mainColor
        pagingViewController.indicatorColor = GlobalSettings.shared().mainColor
        pagingViewController.indicatorOptions = .visible(
            height: 5,
            zIndex: Int.max,
            spacing: .zero,
            insets: .zero
        )
        
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pagingViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            pagingViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }

}
