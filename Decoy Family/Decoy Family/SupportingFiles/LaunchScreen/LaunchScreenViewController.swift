//
//  LaunchScreenViewController.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-16.
//

import UIKit
import AVFoundation

protocol LaunchScreenViewControllerDelegate: AnyObject {
    func launchParchmentViewController()
}

class LaunchScreenViewController: UIViewController {
    
    weak var delegate: LaunchScreenViewControllerDelegate?
    
    var player = AVPlayer()
    
    var playerLayer = AVPlayerLayer()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let urlPathString = Bundle.main.path(forResource: "DFLogo", ofType: "mp4") ?? ""
        configurePlayer(with: urlPathString)
        DispatchQueue.main.asyncAfter(deadline: .now() + 7.5) { [weak self] in
            self?.delegate?.launchParchmentViewController()
        }
    }
    
    func configurePlayer(with url: String) {
        let url = URL(fileURLWithPath: url)
        player = AVPlayer(url: url)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(playerLayer)
        playerLayer.frame = CGRect(x: view.frame.minX, y: view.frame.minY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        player.play()
    }
}
