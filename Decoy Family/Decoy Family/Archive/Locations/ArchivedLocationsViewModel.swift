//
//  ArchivedLocationsViewModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-18.
//

import Foundation

class ArchivedLocationsViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    var locationsIDs = [String]()
    
    var cells = [TableCellViewModel]()
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        getLocationsIDs()
    }
    
    func fetchItems() {
        
        //TODO: map fetched items to swift objects
        
        NetworkManager.shared.apollo.fetch(query: LocationsByIdsQuery(ids: locationsIDs)) { [weak self] response in
            
            switch response {
            case .success(let result):
                guard let items = result.data?.locationsByIds else { return }
                
                let sections = self?.makeSections(items: items)
                
                self?.view?.show(result: .success(sections))
                
            case .failure(let error):
                let error = RMError(rawValue: error.localizedDescription)
                self?.view?.show(result: .failure(error ?? .serverResponse))
                
            }
        }
    }
    
    func getLocationsIDs() {
        
        guard let locations = DBManager.shared?.loadItems(itemType: StorableItem.self) else { return }
        let ids = locations.map({ ($0 as? Persistable)?.publicID })
        
        if let ids = ((ids as? [String])), ids != [] {
            locationsIDs = ids
            fetchItems()
            
        } else {
            view?.show(result: .failure(.noArchivedItem))
        }
    }
    
    func makeSections(items: [LocationsByIdsQuery.Data.LocationsById?]) -> [Sectionable] {
        
        let newCells = items.map({LocationCellViewModel(reuseID: LocationsTableViewCell.reuseID, cellClass: LocationsTableViewCell.self, model: $0)})
        
        let sections = [SectionProvider(cells: newCells, headerView: nil, footerView: nil)]
        
        cells += newCells
        
        return sections
        
        
    }
    
}
