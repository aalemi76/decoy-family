//
//  CharacterGraphQLResult.swift
//  Decoy Family
//
//  Created by AliReza on 2022-06-13.
//

import Foundation

struct CharactersByIDsResponseData: Codable {
    let charactersByIDs: [CharacterModel]
    enum CodingKeys: String, CodingKey {
        case charactersByIDs = "charactersByIds"
    }
}
