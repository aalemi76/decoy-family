//
//  ArchiveCharactersViewModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-18.
//

import Foundation

class ArchiveCharactersViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    var charactersIDs = [String]()
    
    var cells = [TableCellViewModel]()
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        getCharactersIDs()
    }
    
    func fetchItems() {
        
        //TODO: map fetched items to swift object
        
        NetworkManager.shared.apollo.fetch(query: CharactersByIdsQuery(ids: charactersIDs)) { [weak self] response in
            
            switch response {
            case .success(let result):
                guard let items = result.data?.charactersByIds else { return }
                
                let sections = self?.makeSections(items: items)
                
                self?.view?.show(result: .success(sections))
                
            case .failure(let error):
                let error = RMError(rawValue: error.localizedDescription)
                self?.view?.show(result: .failure(error ?? .serverResponse))
                
            }
        }
    }
    
    func getCharactersIDs() {
        
        fetchItems()
        
        guard let characters = DBManager.shared?.loadItems(itemType: StorableItem.self) else { return }
        let ids = characters.map({ ($0 as? Persistable)?.publicID })
        
        if let ids = ((ids as? [String])), ids != [] {
            charactersIDs = ids
            fetchItems()
            
        } else {
            view?.show(result: .failure(.noArchivedItem))
        }
    }
    
    func makeSections(items: [CharactersByIdsQuery.Data.CharactersById?]) -> [Sectionable] {
        
        let newCells = items.map({CharacterCellViewModel(reuseID: CharacterTableViewCell.reuseID, cellClass: CharacterTableViewCell.self, model: $0)})
        
        let sections = [SectionProvider(cells: newCells, headerView: nil, footerView: nil)]
        
        cells += newCells
        
        return sections
        
        
    }
    
}
