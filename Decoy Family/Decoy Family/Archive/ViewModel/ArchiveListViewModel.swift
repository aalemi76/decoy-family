//
//  ArchiveListViewModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import Foundation

protocol ArchiveListViewModelDelegate: AnyObject {
    func didSelectCharactersBtn()
    func didSelectLocationsBtn()
    func didSelectEpisodesBtn()
}

class ArchiveListViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    weak var delegate: ArchiveListViewModelDelegate?
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        fetchItems()
    }
    
    func fetchItems() {
        let cellViewModel = ArchiveCellViewModel(reuseID: ArchiveTableViewCell.reuseID, cellClass: ArchiveTableViewCell.self, model: "")
        cellViewModel.delegate = self
        let cells = [cellViewModel]
        let section = [SectionProvider(cells: cells, headerView: nil, footerView: nil)]
        view?.show(result: .success(section))
    }
    
}
