//
//  ArchiveListViewModel+Ext.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import Foundation

extension ArchiveListViewModel: ArchiveCellViewModelDelegate {
    
    func didSelectCharactersBtn() {
        delegate?.didSelectCharactersBtn()
    }
    
    func didSelectLocationsBtn() {
        delegate?.didSelectLocationsBtn()
    }
    
    func didSelectEpisodesBtn() {
        delegate?.didSelectEpisodesBtn()
    }
    
}
