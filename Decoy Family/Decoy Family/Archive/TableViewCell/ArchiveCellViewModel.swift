//
//  ArchiveCellViewModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import Foundation

protocol ArchiveCellViewModelDelegate: AnyObject {
    func didSelectCharactersBtn()
    func didSelectLocationsBtn()
    func didSelectEpisodesBtn()
}

class ArchiveCellViewModel: TableCellViewModel {
    
    weak var delegate: ArchiveCellViewModelDelegate?
    
    func didSelectCharactersBtn() {
        delegate?.didSelectCharactersBtn()
    }
    
    func didSelectLocationsBtn() {
        delegate?.didSelectLocationsBtn()
    }
    
    func didSelectEpisodesBtn() {
        delegate?.didSelectEpisodesBtn()
    }
}
