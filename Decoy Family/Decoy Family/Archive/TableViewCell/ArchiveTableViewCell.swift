//
//  ArchiveTableViewCell.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import UIKit

class ArchiveTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: ArchiveTableViewCell.self)
    
    @IBOutlet weak var charactersBtn: UIImageView!
    @IBOutlet weak var locationsBtn: UIImageView!
    @IBOutlet weak var episodesBtn: UIImageView!
    
    weak var viewModel: Reusable?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeViewRounded(image: charactersBtn, tapGesture: UITapGestureRecognizer(target: self, action: #selector(didSelectCharactersBtn)))
        makeViewRounded(image: locationsBtn, tapGesture: UITapGestureRecognizer(target: self, action: #selector(didSelectLocationsBtn)))
        makeViewRounded(image: episodesBtn, tapGesture: UITapGestureRecognizer(target: self, action: #selector(didSelectEpisodesBtn)))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
    }
    
    func update(model: Any) {
        return
    }
    
    func makeViewRounded(image: UIImageView, tapGesture: UITapGestureRecognizer) {
        image.isUserInteractionEnabled = true
        image.layer.cornerRadius = 30
        image.clipsToBounds = true
        image.addGestureRecognizer(tapGesture)
    }
    
    @objc func didSelectCharactersBtn() {
        (viewModel as? ArchiveCellViewModel)?.didSelectCharactersBtn()
    }
    
    @objc func didSelectLocationsBtn() {
        (viewModel as? ArchiveCellViewModel)?.didSelectLocationsBtn()
    }
    
    @objc func didSelectEpisodesBtn() {
        (viewModel as? ArchiveCellViewModel)?.didSelectEpisodesBtn()
    }
    
}
