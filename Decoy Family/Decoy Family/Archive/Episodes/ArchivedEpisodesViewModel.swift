//
//  ArchivedEpisodesViewModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-18.
//

import Foundation

class ArchivedEpisodesViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    var episodesIDs = [String]()
    
    var cells = [TableCellViewModel]()
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        getEpisodesIDs()
    }
    
    func fetchItems() {
        
        //TODO: map fetched items to swift object
        
        NetworkManager.shared.apollo.fetch(query: EpisodesByIdsQuery(ids: episodesIDs)) { [weak self] response in
            
            switch response {
            case .success(let result):
                guard let items = result.data?.episodesByIds else { return }
                
                let sections = self?.makeSections(items: items)
                
                self?.view?.show(result: .success(sections))
                
            case .failure(let error):
                let error = RMError(rawValue: error.localizedDescription)
                self?.view?.show(result: .failure(error ?? .serverResponse))
                
            }
        }
    }
    
    func getEpisodesIDs() {
        
        guard let episodes = DBManager.shared?.loadItems(itemType: StorableItem.self) else { return }
        let ids = episodes.map({ ($0 as? Persistable)?.publicID })
        
        if let ids = ((ids as? [String])), ids != [] {
            episodesIDs = ids
            fetchItems()
            
        } else {
            view?.show(result: .failure(.noArchivedItem))
        }
    }
    
    func makeSections(items: [EpisodesByIdsQuery.Data.EpisodesById?]) -> [Sectionable] {
        
        let newCells = items.map({EpisodeCellViewModel(reuseID: EpisodeTableViewCell.reuseID, cellClass: EpisodeTableViewCell.self, model: $0)})
        
        let sections = [SectionProvider(cells: newCells, headerView: nil, footerView: nil)]
        
        cells += newCells
        
        return sections
        
        
    }
    
}
