//
//  DBManager.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import Foundation
import RealmSwift

class DBManager: DataPersistency {
    
    static let shared = DBManager()
    
    private let realm: Realm
    
    typealias ItemModel = StorableItem
    
    init?() {
        do {
            self.realm = try Realm()
        } catch {
            print(error)
            return nil
        }
    }
    
    func save(_ item: StorableItem) {
        do {
            try realm.write {
                realm.add(item)
            }
        } catch {
            print(error)
        }
    }
    
    func loadItem(itemType: StorableItem.Type, itemID: String) -> StorableItem? {
        let items = realm.objects(itemType)
        let item = items.first { item in
            return item.publicID == itemID
        }
        return item
    }
    
    func loadItems(itemType: StorableItem.Type) -> [StorableItem]? {
        return realm.objects(itemType).reversed()
    }
    
    func delete(objects: [StorableItem]) {
        do {
            try realm.write {
                realm.delete(objects)
            }
        } catch {
            print(error)
        }
    }
    
    func deleteDB() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            print(error)
        }
    }
}

