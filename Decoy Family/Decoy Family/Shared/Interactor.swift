//
//  Interactor.swift
//  Decoy Family
//
//  Created by AliReza on 2022-06-12.
//

import Foundation
import Apollo

class Interactor<Model: Codable, Query: GraphQLQuery> {
    typealias Object = Model
    let query: Query
    init(query: Query) {
        self.query = query
    }
    func parse(data: Data) -> Model? {
        let decoder = JSONDecoder()
        let model = try? decoder.decode(Model.self, from: data)
        return model
    }

    func getModel(onSuccess: @escaping (Object) -> Void,
                  onFailure: @escaping (RMError) -> Void) {
        NetworkManager.shared.apollo.fetch(query: query) { response in
            switch response {
            case .success(let graphQLResult):
                let data = try? JSONSerialization.data(withJSONObject: graphQLResult.data?.jsonObject)
                guard let data = data else {
                    onFailure(.serverResponse)
                    return
                }
                guard let model = self.parse(data: data) else {
                    onFailure(.parsingError)
                    return
                }
                onSuccess(model)
            case .failure(let error):
                onFailure(RMError(rawValue: error.localizedDescription ) ?? .serverResponse)
            }
        }
    }
}
