//
//  Sectionable.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import UIKit.UIView
protocol Sectionable: AnyObject {
    init(cells: [Reusable], headerView: UIView?, footerView: UIView?)
    func getCells() -> [Reusable]
    func getHeaderView() -> UIView?
    func getFooterView() -> UIView?
    func append(_ cells: [Reusable])
    func removeCells()
}
