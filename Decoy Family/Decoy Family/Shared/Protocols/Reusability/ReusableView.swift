//
//  ReusableView.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import Foundation
protocol ReusableView {
    func getReuseID() -> String
    func getViewClass() -> AnyClass
}
