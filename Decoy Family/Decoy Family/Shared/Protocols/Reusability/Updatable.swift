//
//  Updatable.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import Foundation
protocol Updatable: AnyObject {
    func attach(viewModel: Reusable)
    func update(model: Any)
}
