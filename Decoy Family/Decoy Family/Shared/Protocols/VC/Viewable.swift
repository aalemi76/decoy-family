//
//  Viewable.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation
protocol Viewable: AnyObject {
    func show(result: Result<Any, RMError>)
    func loadNextPage(result: Result<Any, RMError>)
}

extension Viewable {
    func loadNextPage(result: Result<Any, RMError>) {
        return
    }
}
