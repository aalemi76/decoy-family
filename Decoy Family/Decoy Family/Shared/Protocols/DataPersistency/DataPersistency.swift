//
//  DataPersistency.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import Foundation

protocol DataPersistency: AnyObject {
    associatedtype ItemModel
    func save(_ object: ItemModel)
    func loadItem(itemType: ItemModel.Type, itemID: String) -> ItemModel?
    func loadItems(itemType: ItemModel.Type) -> [ItemModel]?
    func delete(objects: [ItemModel])
    func deleteDB()
}
