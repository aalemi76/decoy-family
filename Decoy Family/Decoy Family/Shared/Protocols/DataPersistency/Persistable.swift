//
//  Persistable.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-17.
//

import Foundation

protocol Persistable {
    var publicID: String {set get}
    var isArchived: Bool {set get}
}
