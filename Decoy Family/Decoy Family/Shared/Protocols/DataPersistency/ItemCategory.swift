//
//  ItemType.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-18.
//

import Foundation

enum ItemCategory: String {
    case character = "Character"
    case location = "Location"
    case episode = "Episode"
    case none = "None"
    
    func isOfType(_ type: ItemCategory) -> Bool {
        return rawValue == type.rawValue
    }
}
