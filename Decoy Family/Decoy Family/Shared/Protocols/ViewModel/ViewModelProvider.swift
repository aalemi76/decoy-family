//
//  ViewModelProvider.swift
//  SilverOrange
//
//  Created by AliReza on 2022-05-08.
//

import Foundation

protocol ViewModelProvider {
    func viewDidLoad(_ view: Viewable)
    func fetchItems()
}
