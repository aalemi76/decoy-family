//
//  RMError.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import Foundation

enum RMError: String, Error {
    case serverResponse = "Unable to communicate with server, please check your connection and try again."
    case parsingError = "Unable to parse received data to the specified model."
    case noArchivedItem = "You have not archived any item yet."
}
