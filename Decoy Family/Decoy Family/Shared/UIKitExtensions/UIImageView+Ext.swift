//
//  UIImageView+Ext.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import UIKit
import SDWebImage

extension UIImageView {
    func loadImage(url: String, placeHolder: UIImage?) {
        
        guard let url = URL(string: url) else { return }
        self.sd_setImage(with: url, placeholderImage: placeHolder)
        
    }
}
