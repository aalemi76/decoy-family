//
//  LocationModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-06-09.
//

import Foundation

class LocationModel: StorableItem, Codable {
    var id: String?
    var name: String?
    var type: String?
    var dimension: String?
    var residents: [CharacterModel]?
}
