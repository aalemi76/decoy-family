//
//  EpisodeModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-06-09.
//

import Foundation

class EpisodeModel: StorableItem, Codable {
    var id: String?
    var name: String?
    var airDate: String?
    var episode: String?
    var characters: [CharacterModel]?
}
