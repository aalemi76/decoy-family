//
//  CharacterModel.swift
//  Decoy Family
//
//  Created by AliReza on 2022-06-09.
//

import Foundation

class CharacterModel: StorableItem, Codable {
    var id: String?
    var name: String?
    var status: String?
    var species: String?
    var type: String?
    var gender: String?
    var origin: LocationModel?
    var location: LocationModel?
    var episode: [EpisodeModel]?
    var image: String?
    var created: String?
}
