//
//  StorableItem.swift
//  Decoy Family
//
//  Created by AliReza on 2022-06-09.
//

import Foundation
import RealmSwift

class StorableItem: Object, Persistable {
    
    
    @Persisted var publicID: String = ""
    @Persisted var isArchived: Bool = true
    
    convenience init(id: String) {
        self.init()
        self.publicID = id
    }
}
