//
//  NetworkManager.swift
//  Decoy Family
//
//  Created by AliReza on 2022-05-12.
//

import Foundation
import Apollo

class NetworkManager {
  static let shared = NetworkManager()

  private(set) lazy var apollo = ApolloClient(url: URL(string: "https://rickandmortyapi.com/graphql")!)
}
